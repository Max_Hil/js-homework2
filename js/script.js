let number
do {
  number = prompt("Enter your number");
} while (number <= 1 || (number % 1) != 0);

outer: for (let i = 2; i <= number; i++) {
 for (let n = 2; n < Math.sqrt(i); n++) {
    if (i % n == 0) continue outer;
  }
 console.log(i);
}
